package day6;

import java.util.LinkedList;
import java.util.List;

public class Day6 {
    public static void main(String[] args) {

    }

    public static List<Coord> loadInput(List<String> input) {
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<Coord> coords = new LinkedList<>();
        for (int i = 0; i < input.size(); i++) {
            Coord c = new Coord(input.get(i));
            if (i >= alpha.length()) {
                // overflow for when input is > 26 rows
                c.setCharacter(String.valueOf(i));
            } else {
                c.setCharacter(alpha.substring(i, i + 1));
            }

            coords.add(c);
        }
        return coords;
    }

    public static List<String> buildGrid(List<Coord> input) {
        //find max x/y
        int max_x = input.stream().mapToInt(Coord::getX).max().orElse(0) + 1;
        int max_y = input.stream().mapToInt(Coord::getY).max().orElse(0) + 1;
        //build empty grid first
        List<String> grid = new LinkedList<>();
        for (int i = 0; i < max_y; i++) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j < max_x; j++) {
                row.append(".");
            }
            grid.add(row.toString());
        }
        //fill in grid
        for (int i = 0; i < input.size(); i++) {
            Coord coord = input.get(i);
            String rowStr = grid.get(coord.getY());
            rowStr = rowStr.substring(0, coord.getX()) + coord.getCharacter() + rowStr.substring(coord.getX() + 1);
            grid.set(coord.getY(), rowStr);
        }
        return grid;
    }

    public static String findClosestCoord(List<Coord> coords, int x, int y) {
        //TODO: implement
        int[][] distances = {};
        return ".";
    }

    public static void printGrid(List<String> grid) {
        grid.forEach(System.out::println);
    }
}
