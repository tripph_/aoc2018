package day6;

import lombok.Data;

@Data
public class Coord {
    private int x;
    private int y;
    private String character;

    public Coord(String in) {
        this.x = Integer.parseInt(in.split(",")[0]);
        this.y = Integer.parseInt(in.split(" ")[1]);
    }
}
