package day4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class Day4 {
    public static void main(String[] args) {
        // part 1
        Map<Integer, Map<Integer, Integer>> data = Day4.getMapOfMapsOfSleepingMinutes(loadRows("src/main/resources/input_4.txt"));
        int sleepiest_guard = Day4.findGuardWithMostMinutesSlept(data);
        int magic_minute = Day4.findMinuteSleptMost(data, sleepiest_guard);
        System.out.println(sleepiest_guard * magic_minute);
    }

    public static int findGuardWithMostMinutesSlept(Map<Integer, Map<Integer, Integer>> data) {
        int top_sleeper = -1;
        int max_sleep = 0;
        for (Map.Entry<Integer, Map<Integer, Integer>> entry : data.entrySet()) {
            int sum = entry.getValue().values().stream().mapToInt(Integer::intValue).sum();
            if (sum > max_sleep) {
                max_sleep = sum;
                top_sleeper = entry.getKey();
            }
        }
        return top_sleeper;
    }

    public static int findMinuteSleptMost(Map<Integer, Map<Integer, Integer>> allData, int guard_id) {
        int max = 0;
        int magic_minute = -1;
        Map<Integer, Integer> data = allData.get(guard_id);
        for (Map.Entry<Integer, Integer> entry : data.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                magic_minute = entry.getKey();
            }
        }
        return magic_minute;
    }
    public static Map<Integer, Integer> findGuardAndMinuteSleptMostFrequently(Map<Integer, Map<Integer, Integer>> allData) {
        int max = 0;
        int magic_minute = -1;
        int sleepy_guard = -1;
        for (Map.Entry<Integer, Map<Integer,Integer>> entry : allData.entrySet()) {
            int this_max_minutes_slept = entry.getValue().entrySet().stream().mapToInt(value -> value.getValue()).max().orElseGet(() -> 0);
            if(this_max_minutes_slept > max) {
                max = this_max_minutes_slept;
                magic_minute = entry.getValue().entrySet().stream().filter(e -> e.getValue() == this_max_minutes_slept).findFirst().get().getKey();
                sleepy_guard = entry.getKey();
            }
        }
        Map res = new HashMap<Integer, Integer>();
        res.put(sleepy_guard, magic_minute);
        return res;
    }


    public static List<Day4Row> loadRows(String filename) {
        List<String> input = null;
        try {
            input = Files.readAllLines(Paths.get(filename));
            return input.stream()
                    .map(Day4Row::new).sorted(Comparator.comparingLong(value -> value.getTimestamp().toEpochSecond(ZoneOffset.UTC))).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Map<Integer, Map<Integer, Integer>> getMapOfMapsOfSleepingMinutes(List<Day4Row> rows) {
        Map results = new HashMap<Integer, HashMap<Integer, Integer>>(); // key==guard_id, value = map<minute, sleeping_count>
        Day4Row[] arr = new Day4Row[rows.size()];
        rows.toArray(arr);
        LocalDateTime fell_asleep_at = null;
        int current_guard = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].getGuard_id() != 0) {
                current_guard = arr[i].getGuard_id();
            }

            if (!results.containsKey(current_guard)) {
                results.put(current_guard, new HashMap<Integer, Integer>());
            }


            if (arr[i].getAction().contains("fall")) {
                //todo: reset
                fell_asleep_at = arr[i].getTimestamp();
            }
            if (arr[i].getAction().contains("wake")) {
                int wt = arr[i].getTimestamp().getMinute();
                int ft = fell_asleep_at.getMinute();
                for (int min = ft; min < wt; min++) {
                    Map<Integer, Integer> prevRes = (Map<Integer, Integer>) results.get(current_guard);
                    if (prevRes == null) {
                        prevRes = new HashMap<Integer, Integer>();
                    }
                    if (prevRes.containsKey(min)) {
                        int prevVal = prevRes.get(min);
                        prevVal++;
                        prevRes.put(min, prevVal);
                    } else {
                        prevRes.put(min, 1);
                    }
                    results.put(current_guard, prevRes);
                }
            }
        }
        return results;
    }
}
