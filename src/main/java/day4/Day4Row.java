package day4;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Day4Row {
    private LocalDateTime timestamp;
    private int guard_id;
    private String action;
    public Day4Row(String in) {
        final String inTS = in.split("]")[0].replace("[","").replace(" ", "T");
        this.timestamp = LocalDateTime.parse(inTS);
        if(in.contains("#")) {
            this.guard_id = Integer.parseInt(in.split("#")[1].split(" ")[0]);
            this.action = in.split(" ")[4];

        } else {
            this.action = in.split(" ")[2];
        }
    }
}
