package day5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day5 {
    public static void main(String[] args) throws IOException {
        String input = Files.readAllLines(Paths.get("src/main/resources/input_5.txt")).get(0);
        System.out.println(runReaction(input).length());
        //part 2
        System.out.println(runPreReactions(input));
    }

    public static String runReaction(String input) {
        //TODO rerun after completed to recheck.
        boolean done = false;
//        while(true) {
//            String newInput = Day5.react(input);
//            if(input == newInput) {
//                return input;
//            } else {
//                input = newInput;
//            }
//        }
        return Day5.react(input);
    }

    public static int runPreReactions(String input) {
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        char[] arrAlpha = alpha.toCharArray();
        int minLength = Integer.MAX_VALUE;
        for (char al : arrAlpha) {
            int l = react(prereact(input, String.valueOf(al))).length();
            if (l < minLength) {
                minLength = l;
            }
        }
        return minLength;

    }

    public static String prereact(String input, String ch) {
        return input.replaceAll(ch.toLowerCase(), "").replaceAll(ch.toUpperCase(), "");
    }

    public static String react(String input) {
        //index string from start
        //find chars that are next to eachother and same letter, but different case

        String lastChar = "";
        for (int i = 0; i < input.length(); i++) {
            if (!lastChar.equals("") && input.substring(i, i + 1).equalsIgnoreCase(lastChar) && !input.substring(i, i + 1).equals(lastChar)) {
                if (i > 1) {
                    input = input.substring(0, i - 1) + input.substring(i + 1);
                    i -= 2;
                    lastChar = input.substring(i, i + 1);
                } else {
                    input = input.substring(i + 1);
                    i = 0;
                    lastChar = input.substring(0, 1);
                }

            } else {
                lastChar = input.substring(i, i + 1);
            }
        }

        return input;
    }

}
