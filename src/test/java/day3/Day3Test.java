package day3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day3Test {

    @Test
    void createFabricGrid() {
        int[][] grid = Day3.createFabricGrid(15,15);
        Day3.printGrid(grid);
        System.out.println();
        String input = "#123 @ 3,2: 5x4";
        Claim claim = new Claim(input);
        grid = Day3.rasterizeFabric(grid, claim);
        grid = Day3.rasterizeFabric(grid, claim);
        Day3.printGrid(grid);
        assertEquals(20, Day3.countOverlaps(grid));

    }
}