import org.junit.jupiter.api.Test;

import java.util.List;

class Day1Test {
    @Test
    public void part2() {



        List<String> input1 = List.of("+1","-1");
        Integer answer1 = 0;

        List<String> input2 = List.of("+3", "+3", "+4", "-2","-4");
        Integer answer2 = 10;

        List<String> input3 = List.of("-6", "+3", "+8", "+5","-6");
        Integer answer3 = 5;
        List<String> input4 = List.of("+7", "+7", "-2", "-7","-4");
        Integer answer4 = 14;

        assert answer1.equals(Day1.findFirstRepeat(input1));
        assert answer2.equals(Day1.findFirstRepeat(input2));
        assert answer3.equals(Day1.findFirstRepeat(input3));
        assert answer4.equals(Day1.findFirstRepeat(input4));



    }
}